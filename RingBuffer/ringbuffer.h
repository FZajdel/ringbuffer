#ifndef _RINGBUFFER_H
#define _RINGBUFFER_H

#include <cstdint>


/** @brief Provides Circular/Ring Buffer functionality */
class RingBuffer{
public:
    /** @brief Initializes buffer of bufferSize 
     * @param bufferSize determines the size of buffer
    */
    RingBuffer(uint32_t bufferSize);
    /** @brief Returns current size of buffer */
    uint32_t getSize();
    /** @brief Returns max size of buffer */
    uint32_t getMaxSize();
private:
    uint32_t maxSize;
    uint32_t size;
};

#endif /* _RINGBUFFER_H */