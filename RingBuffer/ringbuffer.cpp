#include "ringbuffer.h"

RingBuffer::RingBuffer(uint32_t bufferSize) : maxSize(bufferSize){
    size = 0;
}

uint32_t RingBuffer::getMaxSize(){
    return maxSize;
}

uint32_t RingBuffer::getSize(){
    return size;
}