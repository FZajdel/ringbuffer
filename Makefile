# compiler specific
GXX=g++
GXX_FLAGS=-std=c++14 -o0
GXX_AS_FLAGS= $(GXX_FLAGS) -S

# sources
TEST_DIR=Test
TEST_SRC=*.cpp
SRC=RingBuffer/ringbuffer.cpp

# outputs
OUT_DIR=bin
OUT_TEST=$(OUT_DIR)/test
TEST_APP_NAME=ringbuffer-test

test:
	mkdir -p $(OUT_TEST)
	$(GXX) $(GXX_FLAGS) $(SRC) $(TEST_DIR)/$(TEST_SRC) -o $(OUT_TEST)/$(TEST_APP_NAME)
	./$(OUT_TEST)/$(TEST_APP_NAME)

doc:
	doxygen Doxyfile

clean:
	rm -rf $(OUT_DIR)
	rm -rf doc
