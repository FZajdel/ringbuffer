#include "../RingBuffer/ringbuffer.h"
#include <cstdint>
#include <iostream>

#define PRINT_HEADER() std::cout << "\033[1;33m-> \033[0m" << __func__ << ":\t"
#define TEST(expr)                                                             \
  if (expr) {                                                                  \
    std::cout << "\033[1;32mPASSED\033[0m\n";                                  \
  } else {                                                                     \
    std::cout << "\033[1;31mFAILED\033[0m\n";                                  \
  }

void gettingSizeOfBuffer() {
  PRINT_HEADER();

  const int16_t N = 10;
  RingBuffer ringBuffer{N};

  TEST(ringBuffer.getMaxSize() == N);
}

void gettingSizeOfEmptyBuffer() {
  PRINT_HEADER();

  const int16_t N = 10;
  RingBuffer ringBuffer{N};

  TEST(ringBuffer.getSize() == 0);
}

void (*ringBufferTests[])(void) = {gettingSizeOfBuffer,gettingSizeOfEmptyBuffer};

int main(int argc, char *argv[]) {

  std::cout << argv[0] << "\n\n";
  for (auto test : ringBufferTests) {
    test();
  }
  return 0;
}